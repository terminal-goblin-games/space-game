extends Node2D

#nodepaths
onready var spawn = get_parent().get_node("Spawner")

var enemy_spawn_distance_from_player_min = 400
var enemy_spawn_distance_from_player_max = 1000
var enemy_spawn_loacation = Vector2(0,0)
var enemy_spawn_count = 0
var enemy_spawn_count_max = 20
var enemy_spawn_delay = 4.0
var enemy_spawn_delay_max = 1.0
var enemy_spawn_delay_min = 0.5


func check_enemy_count():
	if enemy_spawn_count < enemy_spawn_count_max:
		make_enemy("EnemyType",enemy_spawn_loacation)
		enemy_spawn_count = 0 if enemy_spawn_count < 0 else enemy_spawn_count
		enemy_spawn_count += 1
		enemy_spawn_delay = lerp(enemy_spawn_delay_min,enemy_spawn_delay_max,randf())
	yield(get_tree().create_timer(enemy_spawn_delay), "timeout")
	check_enemy_count()

func make_enemy(type, location):
	spawn.enemy([type, location])

func _ready():
	check_enemy_count()
