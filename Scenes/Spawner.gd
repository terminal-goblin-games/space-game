extends Node2D

var p_projectile = preload("res://Scenes/Projectile.tscn")
var spawn_count = 0

# this is temporary stuff for testing
var p_enemy = preload("res://Scenes/Enemy.tscn")


"""
put the details of projectile parameters here
param[0] projectile type
param[1] spawn position
param[2] direction
param[3] bullet speed
param[4] bullet duration
param[5] bullet color
param[6] target group
param[7] origin group
param[8] damage value
param[9] AoE array
param[10] collision layer
param[11] collision mask

	
"""
func projectile(param): # type, position, bullet_rotation, bullet_color, target_group):
	var new_spawn = p_projectile.instance()
	new_spawn.name = "projectile" + str(spawn_count) + param[0] + str(randi()) # A and B can later be replaced by unique identifiers.  this is to prevent nodes from ending up with the same name
	new_spawn.position = param[1]
	new_spawn.direction = param[2]
	new_spawn.speed = param[3]
	new_spawn.duration = param[4]
	new_spawn.color = param[5]
	new_spawn.target_group = param[6]
	new_spawn.origin_group = param[7]
	new_spawn.damage = param[8]
	new_spawn.collision_layer = param[10]
	new_spawn.collision_mask = param[11]

	get_parent().call_deferred("add_child",new_spawn)
	new_spawn.shoot()

func enemy(param):
	var new_spawn = p_enemy.instance()
	new_spawn.name = "enemy" + str(spawn_count) + param[0] + str(randi()) # A and B can later be replaced by unique identifiers.  this is to prevent nodes from ending up with the same name
	new_spawn.position = param[1]
	get_parent().call_deferred("add_child",new_spawn)
	spawn_count += 1
	$SpawnCount.text = str(spawn_count)
