extends KinematicBody2D

# State machine vars
# May need a second machine eventually if it grows in complexity
enum states {IDLE, SHOOTING}
var state = states.SHOOTING
var flee_counter = 0.0

# Preloads
var p_bullet = preload("res://Scenes/Projectile.tscn")

# combat variables
var health_max = 3000
var health_current = 3000

# steering variables - we should load these from a file later
var arrive_threshold: = 50.0 # how close until you are considered there
var braking_threshhold = 200.0 # how close before you start slowing
var max_speed = 200
var turn_speed = 5.0
var acceleration = 0.5 # currently unused.  use mass instead
var mass = 1.0 # determines acceleration/deceleration
# dont directly mess with these three below here
var velocity = Vector2.ZERO # current velocity 
var turning = 0.0 # current turn_rate 
var desired_destination = Vector2.ZERO # global xy of desired destination

# just for now we will make this the player by default, so they know who to go after on spawn
onready var target_agent = get_parent().get_node("Player") # the current target node
onready var spawn = get_parent().get_node("Spawner")

# orbit 'AI' settings
var orbit_change_counter = 0.0
var orbit_change_interval = 3.0
var orbit_change_interval_max = 1.0
var orbit_change_interval_min = 5.0
var target_orbit_distance = Vector2(500,0) # 
var target_orbit_distance_max = 700.0 # 
var target_orbit_distance_min = 400.0# 
var target_orbit_tolerance = 100.0
var target_orbit_tolerance_max = 200.0
var target_orbit_tolerance_min = 0.0
var target_orbit_speed = 1.0
var target_orbit_speed_max = 3.0
var target_orbit_speed_min = -3.0
var target_orbit_angle = 0.0

# Shooting vars
var rate_of_fire = 2.0
var shot_cooldown = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func state_machine():
	if state == states.IDLE:
		pass
	elif state == states.SHOOTING:
#		look_at(target_agent.global_position)
		if global_position.distance_to(target_agent.global_position) < target_orbit_distance_min/2:
			flee_counter = 0.5
		if flee_counter > 0:
			flee_agent(target_agent)
		else:
			orbit_agent(target_agent)
		if shot_cooldown <= 0:
			shot_cooldown = rate_of_fire
			make_projectile(Vector2(35,0),0,0,400,5.0,"grey","players","enemies",100,[false],96,2)
			
func tick_counters(delta):
	# advances the angle for calculating the target point to orbit.
	# think of this as an invisible clock hand that circles around whatever point you want to place it on
	target_orbit_angle += target_orbit_speed * delta
	orbit_change_counter -= delta
	flee_counter -= delta
	shot_cooldown -= delta
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	tick_counters(delta)
	state_machine()
	velocity = move_and_slide(velocity)
	rotation = set_facing(delta)

func take_damage(dmg):
	health_current -= dmg
	if health_current <= 0:
		get_parent().get_node("EnemyManager").enemy_spawn_count -= 1
		queue_free()

func set_new_target(new_target):
	target_agent = new_target
	# set initial orbit angle to be closest distance
	target_orbit_angle = -1*self.get_angle_to(new_target.global_position)

func set_facing(delta):
	turning = Steering.turn_to(target_agent.global_position,global_position,global_rotation,turn_speed*delta)
	return turning

func orbit_agent(agent):
	check_orbit_change()
	follow_point(agent.global_position + target_orbit_distance.rotated(target_orbit_angle),target_orbit_tolerance)

func flee_agent(agent):
	check_orbit_change()
	follow_point(position + Vector2(target_orbit_distance_min,0).rotated(position.angle_to_point(agent.position)),target_orbit_tolerance)

func check_orbit_change():
	if orbit_change_counter < 0:
		orbit_change_counter = lerp(orbit_change_interval_min, orbit_change_interval_max, randf())
		target_orbit_distance = Vector2(lerp(target_orbit_distance_min,target_orbit_distance_max,randf()),0) # 
		target_orbit_tolerance = lerp(target_orbit_tolerance_min,target_orbit_tolerance_max,randf())
		target_orbit_speed = lerp(target_orbit_speed_min,target_orbit_speed_max,randf())
		target_orbit_angle = lerp(-PI,PI,randf())
	
func follow_point(target_point, follow_offset):
	var target_global_position: Vector2 = target_point
#	var target_global_position: Vector2 = target_agent.global_position
	var to_target = global_position.distance_to(target_global_position)
	if to_target < arrive_threshold:
		return
	var follow_global_position: Vector2 = (
		target_global_position - (target_global_position - global_position).normalized() * follow_offset
		if to_target > follow_offset
		else global_position
	)
	velocity = Steering.arrive_to(
		velocity,
		global_position,
		follow_global_position,
		max_speed, 
		braking_threshhold, # distance to start slowing
		mass  # accelleration/decelleration
	)

func make_projectile(muzzle_position, muzzle_rotation, bullet_rotation, bullet_speed, bullet_duration, bullet_color, target_group, origin_group, bullet_damage, aoe_array, col_layer, col_mask):
	spawn.projectile([
		"bullet", # 0-type
		self.position + muzzle_position.rotated(deg2rad(self.rotation_degrees + muzzle_rotation)), # 1-spawn location
		self.rotation + deg2rad(bullet_rotation), #2-direction
		bullet_speed, #3-speed
		bullet_duration, #4-duration
		bullet_color, #5-color
		["players"], #6-target_group
		["enemies"], #7-origin_group
		bullet_damage, #8-damage
		aoe_array, #9-aoe info
		col_layer, #10-collision layer
		col_mask #11-collision mask
		])
