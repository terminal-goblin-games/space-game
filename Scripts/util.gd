extends Node


static func ArraySort(array_to_sort,column_to_sort) -> Array:
	var array2D = array_to_sort
	var array2DSize = array2D.size()-1
	for j in range(array2DSize):
		for i in range(array2DSize,0+j,-1):
			if (array2D[i][column_to_sort]) < (array2D[i-1][column_to_sort]): #if the statement is true, swap the values
				var temporaryStore = array2D[i-1]
				array2D[i-1] = array2D[i]
				array2D[i] = temporaryStore 
	return array2D
