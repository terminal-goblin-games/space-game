extends KinematicBody2D

#preloads
var p_bullet = preload("res://Scenes/Projectile.tscn")

#nodepaths
onready var spawn = get_parent().get_node("Spawner")

# input variables
var direction = Vector2.ZERO
var velocity = Vector2.ZERO
var move_dir = Vector2.ZERO
var facing = 0

# energy system stats [white,red,yellow,blue]
var energy_color = ["white","red","yellow","blue"]
var energy_value = [10000000,100000,100000,100000]
var energy_max = [10000000,100000,100000,100000]
var energy_boot = [0,75000,75000,75000]
var energy_crash = [0,25000,25000,25000]
var energy_regen = [0,20000,20000,20000]
var energy_active = [1,1,1,1]

# cooldowns
var GCD = 0.0 # global cooldown
var RCD = 0.0 # red cooldown
var YCD = 0.0 # yellow cooldown
var BCD = 0.0 # blue cooldown

# ship stats (import these later)
var hp_max = 10000
var hp_cur = 10000
var move_speed = 300
var slow_speed = 100
var max_speed = 300
var dash_speed = 500
var dash_boost = 1500
var acceleration = 0.5
var deceleration = 0.1
var move_accel = 0.2
var move_decel = 0.1
var dash_accel = 0.3
var dash_decel = 0.075
var slow_accel = 0.1
var slow_decel = 0.5
var orbit_rotate_speed = 1

# button attributes
var ability_button = 0
var utility_button = 0
var dash_engaged = false
var shield_engaged = false
var spinlock_engaged = false
var spinlock_default = false
var spinlock_toggle = true

# Slave variables for eventual network shit
slave var slave_position = Vector2()
slave var slave_velocity = Vector2()
slave var slave_facing = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	$"GUI/HP Bar".max_value = hp_max
	$"GUI/HP Bar".value = hp_cur
	
	$ShieldAnimationPlayer.play("Shield Not Deployed")


func _process(delta):
	if true: #if (is_network_master()):
		check_spinlock()
		check_movement()
		check_defenses()
		check_abilities()
		tick_cooldowns(delta)
		tick_energy(delta)
		do_movement()
		update_hp()
		update_energy_display()
	else: # it is a slave
		do_movement_slave()

func tick_cooldowns(delta):
	GCD = max(GCD-delta,0)
	RCD = max(RCD-delta,0)
	YCD = max(YCD-delta,0)
	BCD = max(BCD-delta,0)

func tick_energy(delta):
	for index in range(1,energy_color.size()):
		var energy_gain = energy_regen[index] * delta if energy_value[0] > 0 else 0
		if energy_value[index] < energy_max[index]:
			energy_value[index] = min(energy_value[index] + energy_gain, energy_max[index])
			energy_value[0] = max(energy_value[0] - energy_gain,0)
		if energy_value[index] >= energy_boot[index]:
			energy_active[index] = true
	if shield_engaged || dash_engaged:
		energy_value[0] -= delta * 10000

# TODO Link this to animation tree6
func _on_ShieldAnimationPlayer_animation_finished(anim_name):
	if anim_name == "Shield Deploy":
		$ShieldAnimationPlayer.play("Shield Fully Deployed")
	elif anim_name == "Shield Retract":
		$ShieldAnimationPlayer.play("Shield Not Deployed")

func take_damage(dmg):
#	hp_cur -= dmg
	
	if hp_cur <= dmg:
		hp_cur = hp_max
		energy_value[0] -= 5000000
	else:
		hp_cur -= dmg

func update_hp():
	$"GUI/HP Bar".value = hp_cur

func update_energy_display():
	$GUI/EnergyLevel/white.value = energy_value[0]
	$GUI/EnergyLevel/red.value = energy_value[1]
	$GUI/EnergyLevel/yellow.value = energy_value[2]
	$GUI/EnergyLevel/blue.value = energy_value[3]
	$GUI/EnergyLevel/white.tint_over = Color(0,0,0,0) if energy_active[0] else Color(0,0,0,0.75)
	$GUI/EnergyLevel/red.tint_over = Color(0,0,0,0) if energy_active[1] else Color(0,0,0,0.75)
	$GUI/EnergyLevel/yellow.tint_over = Color(0,0,0,0) if energy_active[2] else Color(0,0,0,0.75)
	$GUI/EnergyLevel/blue.tint_over = Color(0,0,0,0) if energy_active[3] else Color(0,0,0,0.75)
	
func check_movement():
	move_dir.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	move_dir.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	if move_dir: # if move_dir != Vector2.ZERO:
		if !spinlock_engaged:
			facing = move_dir.angle()
		velocity = lerp(velocity, move_dir.normalized() * max_speed, acceleration)
	else:
		velocity = lerp(velocity, Vector2.ZERO, deceleration)

func check_spinlock():
	if spinlock_toggle:
		if Input.is_action_just_pressed("spinlock"):
			spinlock_engaged = !spinlock_engaged
	else:
		if Input.is_action_pressed("spinlock"):
			spinlock_engaged = false if spinlock_default else true
		else:
			spinlock_engaged = true if spinlock_default else false
	return spinlock_engaged

func check_defenses():
	if Input.is_action_just_pressed("defense"):
		if move_dir: # if there is directional input do a dash
			if !dash_engaged:
				do_dash(true)
				shield_engaged = false
		else:
			if !shield_engaged:
				do_shield(true)
				dash_engaged = false
	elif Input.is_action_just_released("defense"):
		if shield_engaged:
			do_shield(false)
		if dash_engaged:
			do_dash(false)
	elif Input.is_action_pressed("defense"):
		if shield_engaged:
			do_shield(true)
		if dash_engaged:
			do_dash(true)

func check_abilities(): 
	if Input.is_action_just_pressed("button_r") and Input.is_action_just_pressed("button_y") and Input.is_action_just_pressed("button_b"):
		ability_button = "white"
	elif Input.is_action_just_pressed("button_r") and Input.is_action_just_pressed("button_y"):
		ability_button = "orange"
	elif Input.is_action_just_pressed("button_r") and Input.is_action_just_pressed("button_b"):
		ability_button = "purple"
	elif Input.is_action_just_pressed("button_y") and Input.is_action_just_pressed("button_b"):
		ability_button = "green"
	elif Input.is_action_just_pressed("button_r"):
		ability_button = "red"
	elif Input.is_action_just_pressed("button_y"):
		ability_button = "yellow"
	elif Input.is_action_just_pressed("button_b"):
		ability_button = "blue"
	else:
		ability_button = 0
	do_ability(ability_button,utility_button)

# check if required eneregy is available and active
func check_energy(energy_cost_array):
	for index in range(0,energy_color.size()):
		if energy_cost_array[index]:
			if energy_value[index] < energy_cost_array[index] or !energy_active[index]:
				return false
	return true

func spend_energy(energy_cost_array):
	for index in range(0,energy_color.size()):
		energy_value[index] -= energy_cost_array[index]
		if energy_value[index] < energy_crash[index]:
			energy_active[index] = false

func do_movement():
	self.rotation = facing
	move_and_slide(velocity)
	# set slave variables
	#rset("slave_facing", facing)
	#rset("slave_velocity", velocity)
	#rset("slave_position", position)

func do_movement_slave():
	self.rotation = slave_facing
	move_and_slide(slave_velocity)
	position = slave_position


func do_dash(dash_enabled): 
	if dash_enabled: # if dash was just pressed
		if !dash_engaged:
			velocity = (move_dir*dash_boost)
			max_speed = dash_speed
			acceleration = dash_accel
			deceleration = dash_decel
			dash_engaged = true
			energy_value[0] -= 1000 # just for now. eat some white energy
	else: # if dash was just released
		max_speed = lerp(move_speed,max_speed,0.9)
		acceleration = move_accel
		deceleration = move_decel
		dash_engaged = false
# dont forget to set slave dash status eventually
# so we can activate particles and animations in do_defense_slave() at some point

func do_shield(shield_enabled):
	if shield_enabled: # if shield was just pressed
		if !shield_engaged:
			max_speed = slow_speed
			acceleration = slow_accel
			deceleration = slow_decel
			shield_engaged = true
			$ShieldAnimationPlayer.play("Shield Deploy")
	else: # if shield was just released
		$ShieldAnimationPlayer.play("Shield Retract")
		max_speed = move_speed
		acceleration = move_accel
		deceleration = move_decel
		shield_engaged = false
# dont forget to set slave shield status eventually
# so we can activate particles and animations in do_defense_slave() at some point

# Abilities are hard coded for now.  Eventually all of this needs to be divided up into functions, with data loaded based on ship stats.
func do_ability(ability,utility):
	if !GCD:
		var ability_cost_array = [0,0,0,0] # this will be replaced later by ship ability info
		match ability: # eventually these should fire custom abilities based on ship design
			"white":
				if (!RCD && !YCD && !BCD):
					ability_cost_array = [0,50000,50000,50000]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						BCD = 1.0
						YCD = 1.0
						RCD = 1.0
						var idx = 0
						while idx < 360:
							make_projectile(Vector2(35,0),idx,idx,700,2.0,ability,"enemies","allies",1000,[false],48,4)
							idx += 15
			"purple":
				if (!BCD and !RCD):
					ability_cost_array = [0,30000,0,30000]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						RCD = 1.0
						BCD = 1.0
						var idx = 0
						while idx < 360:
							make_projectile(Vector2(35,0),idx,idx,700,2.0,ability,"enemies","allies",1000,[false],48,4)
							idx += 45
						yield(get_tree().create_timer(0.2), "timeout")
						idx = 0
						while idx < 360:
							make_projectile(Vector2(35,0),idx,idx,700,2.0,ability,"enemies","allies",1000,[false],48,4)
							idx += 45
			"green":
				if (!YCD and !BCD):
					ability_cost_array = [0,0,15000,15000]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						YCD = 0.33
						BCD = 0.33
						make_projectile(Vector2(35,0),45,90,700,2.0,ability,"enemies","allies",1000,[false],48,4)
						make_projectile(Vector2(35,0),135,90,700,2.0,ability,"enemies","allies",1000,[false],48,4)
						make_projectile(Vector2(35,0),-45,-90,700,2.0,ability,"enemies","allies",1000,[false],48,4)
						make_projectile(Vector2(35,0),-135,-90,700,2.0,ability,"enemies","allies",1000,[false],48,4)
			"orange":
				if (!RCD and !YCD):
					ability_cost_array = [0,15000,15000,0]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						RCD = 0.25
						YCD = 0.25
						make_projectile(Vector2(35,0),-135,180,700,2.0,ability,"enemies","allies",1000,[false],48,4)
						make_projectile(Vector2(35,0),180,180,700,2.0,ability,"enemies","allies",1000,[false],48,4)
						make_projectile(Vector2(35,0),135,180,700,2.0,ability,"enemies","allies",1000,[false],48,4)
			"red":
				if !RCD:
					ability_cost_array = [0,10000,0,0]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						RCD = 0.1
						make_projectile(Vector2(35,0),0,0,800,2.0,ability,"enemies","allies",1000,[false],48,4)
			"yellow":
				if !YCD:
					ability_cost_array = [0,0,12500,0]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						YCD = 0.2
						make_projectile(Vector2(35,0),45,0,700,2.0,ability,"enemies","allies",1000,[false],48,4)
						make_projectile(Vector2(35,0),-45,0,700,2.0,ability,"enemies","allies",1000,[false],48,4)
			"blue":
				if !BCD:
					ability_cost_array = [0,0,0,12500]
					if check_energy(ability_cost_array):
						spend_energy(ability_cost_array)
						GCD = 0.1
						BCD = 0.33
						var idx = -30
						while idx < 31:
							make_projectile(Vector2(35,0),idx,idx,700,2.0,ability,"enemies","allies",1000,[false],48,4)
							idx += 15
			_:
				pass
# dont forget to set slave ability status eventually
# so we can activate particles and animations in do_ability_slave() at some point
# also need to figure out how we are handling hits on multiplayer


#func make_projectile(muzzle_position, muzzle_rotation, bullet_rotation, bullet_color, target_group):
#	var new_bullet = p_bullet.instance()
#	new_bullet.position = self.position + muzzle_position.rotated(deg2rad(self.rotation_degrees + muzzle_rotation))
#	new_bullet.color = bullet_color
#	get_parent().call_deferred("add_child",new_bullet)
#	new_bullet.shoot(self.rotation + deg2rad(bullet_rotation), target_group)

func make_projectile(muzzle_position, muzzle_rotation, bullet_rotation, bullet_speed, bullet_duration, bullet_color, target_group, origin_group, bullet_damage, aoe_array, col_layer, col_mask):
	spawn.projectile([
		"bullet", # 0-type
		self.position + muzzle_position.rotated(deg2rad(self.rotation_degrees + muzzle_rotation)), # 1-spawn location
		self.rotation + deg2rad(bullet_rotation), #2-direction
		bullet_speed, #3-speed
		bullet_duration, #4-duration
		bullet_color, #5-color
		["enemies"], #6-target_group
		["players"], #7-target_group
		bullet_damage, #8-damage
		aoe_array, #9-aoe info
		col_layer, #10-collision layer
		col_mask #11-collision mask
		])

