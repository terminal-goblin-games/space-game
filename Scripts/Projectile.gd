extends RigidBody2D


var type = "bullet"

var direction = Vector2.ZERO
var speed = 0
var duration = 2.0
var color = "grey"
var target_group = []
var origin_group = []
var damage = 1000
var explosive = false
var exploded = false


# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.modulate = get_color(color)
	$Duration.start(duration)

func get_color(color_text) -> Color:
	var color_value = "#000000"
	match color_text:
		"white": color_value = Color(1,1,1,1.0)
		"red": color_value = Color(1,0,0,1.0)
		"orange": color_value = Color(1,0.5,0,1.0)
		"yellow": color_value = Color(1,1,0,1.0)
		"green": color_value = Color(0,1,0,1.0)
		"blue": color_value = Color(0,0,1,1.0)
		"purple": color_value = Color(0.5,0,1,1.0)
		_: color_value = Color(0.5,0.5,0.5,1.0)
	return color_value

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func shoot():
	apply_impulse(Vector2(), Vector2(speed, 0).rotated(direction))


func _on_Bullet_body_entered(body):
	for t_group in target_group:
		if body.is_in_group(t_group):
			body.take_damage(damage)
			self_destruction()

func self_destruction():
	get_node("CollisionShape2D").set_deferred("disabled",true)
	visible = false
#	if explosive and not exploded:
#		exploded = true
#		MakeAoE(position)
	call_deferred("queue_free")


func _on_Duration_timeout():
	self_destruction()
